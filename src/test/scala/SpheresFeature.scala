package com.craigtreptow.scrayz

import Builders.{point, vector}
import RayUtils._
import MatrixUtils._

class SpheresFeature extends FeatureSpec {
  Scenario("A ray intersects a sphere at two points") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0] = 4.0")
    assert(xs(0).tValue == 4.0)

    And("xs[1] = 6.0")
    assert(xs(1).tValue == 6.0)
  }

  Scenario("A ray intersects a sphere at a tangent") {
    Given("r ← ray(point(0, 1, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 1, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0] = 5.0")
    assert(xs(0).tValue == 5.0)

    And("xs[1] = 5.0")
    assert(xs(1).tValue == 5.0)
  }

  Scenario("A ray misses a sphere") {
    Given("r ← ray(point(0, 2, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 2, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 0")
    assert(xs.length == 0)
  }

  Scenario("A ray originates inside a sphere") {
    Given("r ← ray(point(0, 0, 0), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = 0)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0] = -1.0")
    assert(xs(0).tValue == -1.0)

    And("xs[1] = 1.0")
    assert(xs(1).tValue == 1.0)
  }

  Scenario("A sphere is behind a ray") {
    Given("r ← ray(point(0, 0, 5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = 5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("xs ← intersect(s, r)")
    val xs = intersect(s, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0] = -6.0")
    assert(xs(0).tValue == -6.0)

    And("xs[1] = -4.0")
    assert(xs(1).tValue == -4.0)
  }

  Scenario("A sphere's default transformation") {
    Given("s ← sphere()")
    val s = Sphere()

    Then("s.transform = identity_matrix")
    val s.transform = identityMatrix()
  }

  Scenario("Changing a sphere's transformation") {
    Given("s ← sphere()")
    val s = Sphere()

    And("t ← translation(2, 3, 4)")
    val t = translation(2, 3, 4)

    When("setTransform(s, t)")
    val s2 = s.setTransform(transform = t)

    Then("s.transform = t")
    assert(s2.transform == t)
  }

  Scenario("Intersecting a scaled sphere with a ray") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("setTransform(s, scaling(2, 2, 2)")
    val t = scaling(2, 2, 2)
    val s2 = s.setTransform(transform = t)

    When("xs ← intersect(s, r)")
    val xs = intersect(s2, r)

    Then("xs.count = 2")
    assert(xs.length == 2)

    And("xs[0].t = 3.0")
    assert(xs(0).tValue == 3.0)

    And("xs[1].t = 7.0")
    assert(xs(1).tValue == 7.0)
  }

  Scenario("Intersecting a translated sphere with a ray") {
    Given("r ← ray(point(0, 0, -5), vector(0, 0, 1))")
    val origin = point(x = 0, y = 0, z = -5)
    val direction = vector(x = 0, y = 0, z = 1)
    val r = Ray(origin = origin, direction = direction)

    And("s ← sphere()")
    val s = Sphere()

    When("setTransform(s, translation(5, 0, 0)")
    val t = translation(5, 0, 0)
    val s2 = s.setTransform(transform = t)

    When("xs ← intersect(s, r)")
    val xs = intersect(s2, r)

    Then("xs.count = 0")
    assert(xs.length == 0)
  }

  Scenario("The normal on a sphere at a point on the x axis") {
    Given("s ← sphere()")
    val s = Sphere()

    When("n ← normal_at(s, point(1, 0, 0))")
    val p = point(1, 0, 0)
    val n = normalAt(s, p)

    Then("n = vector(1, 0, 0)")
    val v = vector(1, 0, 0)
    assert(n == v)
  }

  Scenario("The normal on a sphere at a point on the y axis") {
    Given("s ← sphere()")
    val s = Sphere()

    When("n ← normal_at(s, point(0, 1, 0))")
    val p = point(0, 1, 0)
    val n = normalAt(s, p)

    Then("n = vector(0, 1, 0)")
    val v = vector(0, 1, 0)
    assert(n == v)
  }

  Scenario("The normal on a sphere at a point on the z axis") {
    Given("s ← sphere()")
    val s = Sphere()

    When("n ← normal_at(s, point(0, 0, 1))")
    val p = point(0, 0, 1)
    val n = normalAt(s, p)

    Then("n = vector(0, 0, 1)")
    val v = vector(0, 0, 1)
    assert(n == v)
  }

  Scenario("The normal on a sphere at a nonaxial point") {
    Given("s ← sphere()")
    val s = Sphere()

    When("n ← normal_at(s, point(√3/3, √3/3, √3/3))")
    val srtot = Math.sqrt(3) / 3
    val p = point(srtot, srtot, srtot)
    val n = normalAt(s, p)

    Then("n = vector(√3/3, √3/3, √3/3)")
    val v = vector(srtot, srtot, srtot)
    assert(n == v)
  }

  Scenario("The normal is a normalized vector") {
    Given("s ← sphere()")
    val s = Sphere()

    When("n ← normal_at(s, point(√3/3, √3/3, √3/3))")
    val srtot = Math.sqrt(3) / 3
    val p = point(srtot, srtot, srtot)
    val n = normalAt(s, p)

    Then("n = normalize(n)")
    val nn = n.normalize
    assert(n == nn)
  }

  Scenario("Computing the normal on a translated sphere") {
    Given("s ← sphere()")
    val s = Sphere()

    And("set_transform(s, translation(0, 1, 0))")
    val t = translation(0, 1, 0)
    val ss = s.setTransform(t)

    When("n ← normal_at(s, point(0, 1.70711, -0.70711))")
    val p = point(0, 1.70711, -0.70711)
    val n = normalAt(ss, p)

    Then("n = vector(0, 0.70711, -0.70711)")
    val v = vector(0, 0.70711, -0.70711)
    assert(n == v)
  }

  Scenario("Computing the normal on a transformed sphere") {
    Given("s ← sphere()")
    val s = Sphere()

    And("m ← scaling(1, 0.5, 1) * rotation_z(π/5)")
    val scale = scaling(1, 0.5, 1)
    val rotate = rotationZ(Math.PI / 5)
    val m = scale * rotate

    And("set_transform(s, m)")
    val ss = s.setTransform(m)

    When("n ← normal_at(s, point(0, √2/2, -√2/2))")
    val p = point(0, Math.sqrt(2) / 2, -(Math.sqrt(2)) / 2)
    val n = normalAt(ss, p)

    Then("n = vector(0, 0.97014, -0.24254)")
    val v = vector(0, 0.97014, -0.24254)
    assert(n == v)
  }

  Scenario("A sphere has a default material") {
    Given("s ← sphere()")
    val s = Sphere()

    When("m ← s.material")
    val m = s.material

    Then("m = material()")
    assert(m == Material())
  }

  Scenario("A sphere may be assigned a material") {
    Given("s ← sphere()")
    val s = Sphere()

    When("m ← material")
    val m = Material(ambient = 1)

    When("s.material ← m")
    val ss = s.copy(material = m)

    Then("s.material = m")
    assert(ss.material == m)
  }

//  Scenario: A helper for producing a sphere with a glassy material
//  Given s ← glass_sphere()
//  Then s.transform = identity_matrix
//  And s.material.transparency = 1.0
//  And s.material.refractive_index = 1.5
}
