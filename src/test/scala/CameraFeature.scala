package com.craigtreptow.scrayz

import Builders.{point, vector, defaultWorld}
import MatrixUtils.{identityMatrix, rotationY, translation, viewTransform}
import RayUtils.{transform, rayForPixel}
import CanvasUtils.{pixelAt}
import com.craigtreptow.scrayz.{Utils => U}

class CameraFeature extends FeatureSpec {
  Scenario("Constructing a camera") {
    Given("hsize ← 160")
    val hs = 160

    And("vsize ← 120")
    val vs = 120

    And("field_of_view ← π/2")
    val fov = Math.PI / 2

    When("c ← camera(hsize, vsize, field_of_view)")
    val c = Camera(hSize = hs, vSize = vs, fieldOfView = fov)

    Then("c.hsize = 160")
    assert(c.hSize == 160)

    And("c.vsize = 120")
    assert(c.vSize == 120)

    And("c.field_of_view = π/2")
    val pot = Math.PI / 2
    assert(c.fieldOfView == pot)

    And("c.transform = identity_matrix")
    assert(c.transform == identityMatrix())
  }

  Scenario("The pixel size for a horizontal canvas") {
    Given("c ← camera(200, 125, π/2)")
    val c = Camera(200, 125, Math.PI / 2)

    Then("c.pixel_size = 0.01")
    assert(U.equals(c.pixelSize, 0.01))
  }

  Scenario("The pixel size for a vertical canvas") {
    Given("c ← camera(125, 200, π/2)")
    val c = Camera(125, 200, Math.PI / 2)

    Then("c.pixel_size = 0.01")
    assert(U.equals(c.pixelSize, 0.01))
  }

  Scenario("Constructing a ray through the center of the canvas") {
    Given("c ← camera(201, 101, π/2)")
    val c = Camera(201, 101, Math.PI / 2)

    When("r ← ray_for_pixel(c, 100, 50)")
    val r = rayForPixel(camera = c, px = 100, py = 50)

    Then("r.origin = point(0, 0, 0)")
    And("r.direction = vector(0, 0, -1)")
  }

  Scenario("Constructing a ray through a corner of the canvas") {
    Given("c ← camera(201, 101, π/2)")
    val c = Camera(201, 101, Math.PI / 2)

    When("r ← ray_for_pixel(c, 0, 0)")
    val r = rayForPixel(camera = c, px = 0, py = 0)

    Then("r.origin = point(0, 0, 0)")
    assert(r.origin == point(0, 0, 0))

    And("r.direction = vector(0.66519, 0.33259, -0.66851)")
    assert(r.direction == vector(0.66519, 0.33259, -0.66851))
  }

  Scenario("Constructing a ray when the camera is transformed") {
    Given("c ← camera(201, 101, π/2)")
    When("c.transform ← rotation_y(π/4) * translation(0, -2, 5)")
    val rot = rotationY(Math.PI / 4)
    val trans = translation(0, -2, 5)
    val cameraTransform = rot * trans
    val c = Camera(201, 101, Math.PI / 2, transform = cameraTransform)

    And("r ← ray_for_pixel(c, 100, 50)")
    val r = rayForPixel(camera = c, px = 100, py = 50)

    Then("r.origin = point(0, 2, -5)")
    assert(r.origin == point(0, 2, -5))

    And("r.direction = vector(√2/2, 0, -√2/2)")
    val sqrtot = Math.sqrt(2) / 2
    assert(r.direction == vector(sqrtot, 0, -sqrtot))
  }

  Scenario("Rendering a world with a camera") {
    Given("w ← default_world()")
    val w = defaultWorld()

    And("c ← camera(11, 11, π/2)")
    val c = Camera(11, 11, Math.PI / 2)

    And("from ← point(0, 0, -5)")
    val from = point(0, 0, -5)

    And("to ← point(0, 0, 0)")
    val to = point(0, 0, 0)

    And("up ← vector(0, 1, 0)")
    val up = vector(0, 1, 0)

    And("c.transform ← view_transform(from, to, up)")
    val trans = viewTransform(from, to, up)
    val cc = c.copy(transform = trans)

    When("image ← render(c, w)")
    val image = U.render(cc, w)

    Then("pixel_at(image, 5, 5) = color(0.38066, 0.47583, 0.2855)")
    assert(pixelAt(canvas = image, x = 5, y = 5) == Color(0.38066, 0.47583, 0.2855))
  }
}
