package com.craigtreptow.scrayz

import org.scalatest.BeforeAndAfter
import Builders.{point, vector}
import MaterialUtils._

class MaterialsFeature extends FeatureSpec with BeforeAndAfter {
  var m: Material = _
  var position: Tuple = _

  before {
    Given("m ← material()")
    m = Material()

    And("position ← point(0,0,0)")
    position = point(0, 0, 0)
  }

  Scenario("The default material") {
    Then("m.color = color(1,1,1)")
    assert(m.color == Color(red = 1, green = 1, blue = 1))

    Then("m.ambient = 0.1")
    assert(m.ambient == 0.1)

    Then("m.diffuse = 0.9")
    assert(m.diffuse == 0.9)

    Then("m.specular = 0.9")
    assert(m.specular == 0.9)

    Then("m.shininess = 200.0")
    assert(m.shininess == 200.0)
  }

  Scenario("Lighting with the eye between the light and the surface") {
    Given("eyev ← vector(0, 0, -1)")
    val eyeV = vector(0, 0, -1)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And("light ← point_light(point(0, 0, -10), color(1, 1, 1))")
    val light = PointLight(point(0, 0, -10), Color(1, 1, 1))

    When("result ← lighting(m, light, position, eyev, normalv)")
    val result = lighting(m, light, position, eyeV, normalV)

    Then("result = color(1.9, 1.9, 1.9)")
    assert(result == Color(1.9, 1.9, 1.9))
  }

  Scenario("Lighting with the eye between light and surface, eye offset 45°") {
    Given("eyev ← vector(0, √2/2, -√2/2)")
    val sqrtot = Math.sqrt(2) / 2
    val eyeV = vector(0, sqrtot, -sqrtot)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And("light ← point_light(point(0, 0, -10), color(1, 1, 1))")
    val light = PointLight(point(0, 0, -10), Color(1, 1, 1))

    When("result ← lighting(m, light, position, eyev, normalv)")
    val result = lighting(m, light, position, eyeV, normalV)

    Then("result = color(1.0, 1.0, 1.0)")
    assert(result == Color(1.0, 1.0, 1.0))
  }

  Scenario("Lighting with eye opposite surface, light offset 45°") {
    Given("eyev ← vector(0, 0, -1)")
    val eyeV = vector(0, 0, -1)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And(" light ← point_light(point(0, 10, -10), color(1, 1, 1))")
    val light = PointLight(point(0, 10, -10), Color(1, 1, 1))

    When(" result ← lighting(m, light, position, eyev, normalv)")
    val result = lighting(m, light, position, eyeV, normalV)

    Then(" result = color(0.7364, 0.7364, 0.7364)")
    assert(result == Color(0.7364, 0.7364, 0.7364))
  }

  Scenario("Lighting with eye in the path of the reflection vector") {
    Given(" eyev ← vector(0, -√2/2, -√2/2)")
    val sqrtot = Math.sqrt(2) / 2
    val eyeV = vector(0, -sqrtot, -sqrtot)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And(" light ← point_light(point(0, 10, -10), color(1, 1, 1))")
    val light = PointLight(point(0, 10, -10), Color(1, 1, 1))

    When(" result ← lighting(m, light, position, eyev, normalv)")
    val result = lighting(m, light, position, eyeV, normalV)

    Then(" result = color(1.6364, 1.6364, 1.6364)")
    assert(result == Color(1.6364, 1.6364, 1.6364))
  }

  Scenario("Lighting with the light behind the surface") {
    Given("eyev ← vector(0, 0, -1)")
    val eyeV = vector(0, 0, -1)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And(" light ← point_light(point(0, 0, 10), color(1, 1, 1))")
    val light = PointLight(point(0, 0, 10), Color(1, 1, 1))

    When(" result ← lighting(m, light, position, eyev, normalv)")
    val result = lighting(m, light, position, eyeV, normalV)

    Then(" result = color(0.1, 0.1, 0.1)")
    assert(result == Color(0.1, 0.1, 0.1))
  }

  Scenario("Lighting with the surface in shadow") {
    Given("eyev ← vector(0, 0, -1)")
    val eyeV = vector(0, 0, -1)

    And("normalv ← vector(0, 0, -1)")
    val normalV = vector(0, 0, -1)

    And(" light ← point_light(point(0, 0, -10), color(1, 1, 1))")
    val light = PointLight(point(0, 0, -10), Color(1, 1, 1))

    And(" in_shadow ← true")
    val inShadow = true

    When(" result ← lighting(m, light, position, eyev, normalv, in_shadow)")
    val result = lighting(m, light, position, eyeV, normalV, inShadow)

    Then(" result = color(0.1, 0.1, 0.1)")
    assert(result == Color(0.1, 0.1, 0.1))
  }

//  Scenario: Lighting with a pattern applied
//  Given m.pattern ← stripe_pattern(color(1, 1, 1), color(0, 0, 0))
//  And m.ambient ← 1
//  And m.diffuse ← 0
//  And m.specular ← 0
//  And eyev ← vector(0, 0, -1)
//  And normalv ← vector(0, 0, -1)
//  And light ← point_light(point(0, 0, -10), color(1, 1, 1))
//  When c1 ← lighting(m, light, point(0.9, 0, 0), eyev, normalv, false)
//  And c2 ← lighting(m, light, point(1.1, 0, 0), eyev, normalv, false)
//  Then c1 = color(1, 1, 1)
//  And c2 = color(0, 0, 0)
//
//  Scenario: Reflectivity for the default material
//  Given m ← material()
//  Then m.reflective = 0.0
//
//  Scenario: Transparency and Refractive Index for the default material
//  Given m ← material()
//  Then m.transparency = 0.0
//  And m.refractive_index = 1.0
//
}
