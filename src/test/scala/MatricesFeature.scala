package com.craigtreptow.scrayz

import MatrixUtils._

class MatricesFeature extends FeatureSpec {
  Feature("Matrices") {
    Scenario("Constructing and inspecting a 4x4 matrix") {
      //    0   1   2   3
      // 0 a_a a_b a_c a_d
      // 1 b_a b_b b_c b_d
      // 2 c_a c_b c_c c_d
      // 3 d_a d_b d_c d_d
      Given(
        "the following 4x4 matrix M:\n" +
          "|  1   |  2   |  3   |  4   |\n" +
          "|  5.5 |  6.5 |  7.5 |  8.5 |\n" +
          "|  9   | 10   | 11   | 12   |\n" +
          "| 13.5 | 14.5 | 15.5 | 16.5 |"
      )
      val initialValues = Vector(1, 2, 3, 4, 5.5, 6.5, 7.5, 8.5, 9, 10, 11, 12, 13.5, 14.5, 15.5, 16.5)
      val m = Matrix(4, initialValues)

      Then("M[0,0] = 1")
      assert(m.matrix(0)(0) == 1)

      And("M[0,3] = 4")
      assert(m.matrix(0)(3) == 4)

      Then("M[1,0] = 5.5")
      assert(m.matrix(1)(0) == 5.5)

      Then("M[1,2] = 7.5")
      assert(m.matrix(1)(2) == 7.5)

      Then("M[2,2] = 11")
      assert(m.matrix(2)(2) == 11)

      Then("M[3,0] = 13.5")
      assert(m.matrix(3)(0) == 13.5)

      Then("M[3,2] = 15.5")
      assert(m.matrix(3)(2) == 15.5)
    }

    Scenario("A 3x3 matrix ought to be representable") {
      //    0   1   2
      // 0 a_a a_b a_c
      // 1 b_a b_b b_c
      // 2 c_a c_b c_c

      Given(
        "the following 3x3 matrix M:\n" +
          "| -3 |  5 | 0 |\n" +
          "|  1 | -2 | -7 |\n" +
          "| 0 | 1 | 1 |"
      )
      val initialValues = Vector[Double](-3, 5, 0, 1, -2, -7, 0, 1, 1)
      val m = Matrix(3, initialValues)

      Then("M[0,0] = -3")
      assert(m.matrix(0)(0) == -3)

      And("M[1,1] = -2")
      assert(m.matrix(1)(1) == -2)

      Then("M[2,2] = 1")
      assert(m.matrix(2)(2) == 1)
    }

    Scenario("A 2x2 matrix ought to be representable") {
      //    0   1
      // 0 a_a a_b
      // 1 b_a b_b

      Given(
        "the following 2x2 matrix M:\n" +
          "| -3 |  5 |\n" +
          "|  1 | -2 |"
      )
      val initialValues = Vector[Double](-3, 5, 1, -2)
      val m = Matrix(2, initialValues)

      Then("M[0,0] = -3")
      assert(m.matrix(0)(0) == -3)

      And("M[0,1] = 5")
      assert(m.matrix(0)(1) == 5)

      Then("M[1,0] = 1")
      assert(m.matrix(1)(0) == 1)

      Then("M[1,1] = -2")
      assert(m.matrix(1)(1) == -2)
    }

    Scenario("Matrix equality with identical matrices") {
      val initialValues =
        Vector[Double](1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2)
      Given(
        "the following matrix A:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )

      val a = Matrix(4, initialValues)

      Given(
        "the following matrix B:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )
      val b = Matrix(4, initialValues)

      Then("A = B")
      assert(a == b)
    }

    Scenario("Matrix equality with different matrices") {
      val initialValuesA =
        Vector[Double](1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2)
      Given(
        "the following matrix A:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )

      val a = Matrix(4, initialValuesA)

      Given(
        "the following matrix B:\n" +
          "| 2 | 3 | 4 | 5 |\n" +
          "| 6 | 7 | 8 | 9 |\n" +
          "| 8 | 7 | 6 | 5 |\n" +
          "| 4 | 3 | 2 | 1 |"
      )
      val initialValuesB =
        Vector[Double](2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1)
      val b = Matrix(4, initialValuesB)

      Then("A != B")
      assert(a != b)
    }

    Scenario("Matrix equality with small differences in matrices") {
      val initialValuesA =
        Vector[Double](1.00001, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2)
      val initialValuesB =
        Vector[Double](1.00002, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2)

      Given(
        "the following matrix A:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )

      val a = Matrix(4, initialValuesA)

      Given(
        "the following matrix B:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )
      val b = Matrix(4, initialValuesB)

      Then("A = B")
      assert(a == b)
    }

    Scenario("Multiplying two matrices") {
      Given(
        "the following matrix A:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 5 | 6 | 7 | 8 |\n" +
          "| 9 | 8 | 7 | 6 |\n" +
          "| 5 | 4 | 3 | 2 |"
      )

      val initialValuesA =
        Vector[Double](1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2)
      val a = Matrix(4, initialValuesA)

      Given(
        "the following matrix B:\n" +
          "| -2 | 1 | 2 |  3 |\n" +
          "|  3 | 2 | 1 | -1 |\n" +
          "| 4  | 3 | 6 |  5 |\n" +
          "| 1  | 2 | 7 |  8 |"
      )
      val initialValuesB =
        Vector[Double](-2, 1, 2, 3, 3, 2, 1, -1, 4, 3, 6, 5, 1, 2, 7, 8)
      val b = Matrix(4, initialValuesB)

      Then(
        "A * B is the following 4x4 matrix:\n" +
          "| 20 | 22 |  50 |  48 |\n" +
          "| 44 | 54 | 114 | 108 |\n" +
          "| 40 | 58 | 110 | 102 |\n" +
          "| 16 | 26 |  46 |  42 |"
      )

      val expectedValues =
        Vector[Double](20, 22, 50, 48, 44, 54, 114, 108, 40, 58, 110, 102, 16, 26, 46, 42)
      val expected = Matrix(4, expectedValues)
      val calculated = a * b

      assert(calculated == expected)
    }

    Scenario("A matrix multiplied by a tuple") {
      Given(
        "the following matrix A:\n" +
          "| 1 | 2 | 3 | 4 |\n" +
          "| 2 | 4 | 4 | 2 |\n" +
          "| 8 | 6 | 4 | 1 |\n" +
          "| 0 | 0 | 0 | 1 |"
      )

      val initialValues =
        Vector[Double](1, 2, 3, 4, 2, 4, 4, 2, 8, 6, 4, 1, 0, 0, 0, 1)
      val a = Matrix(4, initialValues)

      Given("b ← tuple(1, 2, 3, 1)")
      val b = Tuple(x = 1, y = 2, z = 3, w = 1)

      Then("A * b = tuple(18, 24, 33, 1)")
      val expected = Tuple(x = 18, y = 24, z = 33, w = 1)
      val calculated = a * b

      assert(calculated == expected)
    }

    Scenario("Multiplying a matrix by the identity matrix") {
      Given(
        "the following matrix A:\n" +
          "| 0 | 1 | 2 | 4 |\n" +
          "| 1 | 2 | 4 | 8 |\n" +
          "| 2 | 4 | 8 | 16 |\n" +
          "| 4 | 8 | 16 | 32 |"
      )

      val initialValuesA =
        Vector[Double](0, 1, 2, 4, 1, 2, 4, 8, 2, 4, 8, 16, 4, 8, 16, 32)
      val a = Matrix(4, initialValuesA)

      Then("A * identity_matrix = A")
      val i = identityMatrix()
      val expected = a
      val calculated = a * i

      assert(calculated == expected)
    }

    Scenario("Multiplying the identity matrix by a tuple") {
      Given("a ← tuple(1, 2, 3, 4)")
      val a = Tuple(x = 1, y = 2, z = 3, w = 4)

      Then("identity_matrix * a = a")
      val i = identityMatrix()
      val expected = a
      val calculated = i * a

      assert(calculated == expected)
    }

    Scenario("Transposing a matrix") {
      Given(
        "the following matrix A:\n" +
          "| 0 | 9 | 3 | 0 |\n" +
          "| 9 | 8 | 0 | 8 |\n" +
          "| 1 | 8 | 5 | 3 |\n" +
          "| 0 | 0 | 5 | 8 |"
      )

      val initialValuesA =
        Vector[Double](0, 9, 3, 0, 9, 8, 0, 8, 1, 8, 5, 3, 0, 0, 5, 8)
      val a = Matrix(4, initialValuesA)

      Then(
        "transpose(A) is the following matrix\n" +
          "| 0 | 9 | 1 | 0 |\n" +
          "| 9 | 8 | 8 | 0 |\n" +
          "| 3 | 0 | 5 | 5 |\n" +
          "| 0 | 8 | 3 | 8 |"
      )

      val initialValuesExpected =
        Vector[Double](0, 9, 1, 0, 9, 8, 8, 0, 3, 0, 5, 5, 0, 8, 3, 8)
      val expected = Matrix(4, initialValuesExpected)
      val calculated = transpose(a)

      assert(calculated == expected)
    }

    Scenario("Transposing the identity matrix") {
      Given("A ← transpose(identity_matrix)")
      val calculated = transpose(identityMatrix())

      Then("A = identity_matrix")
      val expected = identityMatrix()
      assert(calculated == expected)
    }

    Scenario("Calculating the determinant of a 2x2 matrix") {
      Given(
        "the following 2x2 matrix A:\n" +
          "|  1 | 5 |\n" +
          "| -3 | 2 |"
      )
      val initialValues = Vector[Double](1, 5, -3, 2)
      val m = Matrix(2, initialValues)
      val calculated = determinant(m)

      Then("determinant(A) = 17")
      assert(calculated == 17)
    }

    Scenario("A submatrix of a 3x3 matrix is a 2x2 matrix") {
      //    0   1   2
      // 0 a_a a_b a_c
      // 1 b_a b_b b_c
      // 2 c_a c_b c_c

      Given(
        "the following 3x3 matrix M:\n" +
          "|  1 | 5 |  0 |\n" +
          "| -3 | 2 |  7 |\n" +
          "|  0 | 6 | -3 |"
      )
      val initialValues = Vector[Double](1, 5, 0, -3, 2, 7, 0, 6, -3)
      val a = Matrix(3, initialValues)

      Then(
        "submatrix(A, 0, 2) is the following 2x2 matrix:\n" +
          "| -3 | 2 |\n" +
          "|  0 | 6 |"
      )
      val initialValuesExpected = Vector[Double](-3, 2, 0, 6)
      val expected = Matrix(2, initialValuesExpected)
      val calculated = submatrix(a, 0, 2)

      assert(calculated == expected)
    }

    Scenario("A submatrix of a 4x4 matrix is a 3x3 matrix") {
      Given(
        "the following matrix A:\n" +
          "| -6 | 1 |  1 | 6 |\n" +
          "| -8 | 5 |  8 | 6 |\n" +
          "| -1 | 0 |  8 | 2 |\n" +
          "| -7 | 1 | -1 | 1 |"
      )

      val initialValuesA =
        Vector[Double](-6, 1, 1, 6, -8, 5, 8, 6, -1, 0, 8, 2, -7, 1, -1, 1)
      val a = Matrix(4, initialValuesA)

      Then(
        "submatrix(A, 2, 1) is the following 3x3 matrix:\n" +
          "| -6 |  1 | 6 |\n" +
          "| -8 |  8 | 6 |\n" +
          "| -7 | -1 | 1"
      )
      val initialValuesExpected = Vector[Double](-6, 1, 6, -8, 8, 6, -7, -1, 1)
      val expected = Matrix(3, initialValuesExpected)
      val calculated = submatrix(a, 2, 1)

      assert(calculated == expected)
    }

    Scenario("Calculating a minor of a 3x3 matrix") {
      Given(
        "the following 3x3 matrix A:\n" +
          "| 3 |  5 |  0 |\n" +
          "| 2 | -1 | -7 |\n" +
          "| 6 | -1 |  5 |"
      )
      val initialValues = Vector[Double](3, 5, 0, 2, -1, -7, 6, -1, 5)
      val a = Matrix(3, initialValues)

      And("B ← submatrix(A, 1, 0)")
      val b = submatrix(a, 1, 0)

      Then("determinant(B) = 25")
      assert(determinant(b) == 25)

      Then("minor(A, 1, 0) = 25")
      assert(minor(a, 1, 0) == 25)
    }

    Scenario("Calculating a cofactor of a 3x3 matrix") {
      Given(
        "the following 3x3 matrix A:\n" +
          "| 3 |  5 |  0 |\n" +
          "| 2 | -1 | -7 |\n" +
          "| 6 | -1 |  5 |"
      )
      val initialValues = Vector[Double](3, 5, 0, 2, -1, -7, 6, -1, 5)
      val a = Matrix(3, initialValues)

      Then("minor(A, 0, 0) = -12")
      assert(minor(a, 0, 0) == -12)

      And("cofactor(A, 0, 0) = -12")
      assert(cofactor(a, 0, 0) == -12)

      Then("minor(A, 1, 0) = 25")
      assert(minor(a, 1, 0) == 25)

      And("cofactor(A, 1, 0) = -25")
      assert(cofactor(a, 1, 0) == -25)
    }

    Scenario("Calculating the determinant of a 3x3 matrix") {
      Given(
        "the following 3x3 matrix A:\n" +
          "|  1 | 2 |  6 |\n" +
          "| -5 | 8 | -4 |\n" +
          "|  2 | 6 |  4 |"
      )
      val initialValues = Vector[Double](1, 2, 6, -5, 8, -4, 2, 6, 4)
      val a = Matrix(3, initialValues)

      Then("cofactor(A, 0, 0) = 56")
      assert(cofactor(a, 0, 0) == 56)

      And("cofactor(A, 0, 1) = 12")
      assert(cofactor(a, 0, 1) == 12)

      Then("cofactor(A, 0, 2) = -46")
      assert(cofactor(a, 0, 2) == -46)

      And("determinant(A) = -196")
      assert(determinant(a) == -196)
    }

    Scenario("Calculating the determinant of a 4x4 matrix") {
      Given(
        "the following 4x4 matrix A:\n" +
          "| -2 | -8 |  3 |  5 |\n" +
          "| -3 |  1 |  7 |  3 |\n" +
          "|  1 |  2 | -9 |  6 |\n" +
          "| -6 |  7 |  7 | -9 |"
      )
      val initialValues =
        Vector[Double](-2, -8, 3, 5, -3, 1, 7, 3, 1, 2, -9, 6, -6, 7, 7, -9)
      val a = Matrix(4, initialValues)

      Then("cofactor(A, 0, 0) = 690")
      assert(cofactor(a, 0, 0) == 690)

      And("cofactor(A, 0, 1) = 447")
      assert(cofactor(a, 0, 1) == 447)

      Then("cofactor(A, 0, 2) = -210")
      assert(cofactor(a, 0, 2) == 210)

      And("cofactor(A, 0, 3) = 51")
      assert(cofactor(a, 0, 3) == 51)

      And("determinant(A) = -4071")
      assert(determinant(a) == -4071)
    }

    Scenario("Testing an invertible matrix for invertibility") {
      Given(
        "the following 4x4 matrix A:\n" +
          "| 6 |  4 | 4 |  4 |\n" +
          "| 5 |  5 | 7 |  6 |\n" +
          "| 4 | -9 | 3 | -7 |\n" +
          "| 9 |  1 | 7 | -6 |"
      )
      val initialValues =
        Vector[Double](6, 4, 4, 4, 5, 5, 7, 6, 4, -9, 3, -7, 9, 1, 7, -6)
      val a = Matrix(4, initialValues)

      Then("determinant(A) = -2120")
      assert(determinant(a) == -2120)

      And("A is invertible")
      assert(invertible(a) == true)
    }

    Scenario("Testing a noninvertible matrix for invertibility") {
      Given(
        "the following 4x4 matrix A:\n" +
          "| -4 |  2 | -2 | -3 |\n" +
          "|  9 |  6 |  2 |  6 |\n" +
          "|  0 | -5 |  1 | -5 |\n" +
          "|  0 |  0 |  0 |  0 |"
      )
      val initialValues =
        Vector[Double](-4, 2, -2, -3, 9, 6, 2, 6, 0, -5, 1, -5, 0, 0, 0, 0)
      val a = Matrix(4, initialValues)

      Then("determinant(A) = 0")
      assert(determinant(a) == 0)

      And("A is not invertible")
      assert(invertible(a) == false)
    }

    Scenario("Calculating the inverse of a matrix") {
      Given(
        "the following 4x4 matrix A:\n" +
          "| -5 |  2 |  6 | -8 |\n" +
          "|  1 | -5 |  1 |  8 |\n" +
          "|  7 |  7 | -6 | -7 |\n" +
          "|  1 | -3 |  7 |  4 |"
      )
      val initialValues =
        Vector[Double](-5, 2, 6, -8, 1, -5, 1, 8, 7, 7, -6, -7, 1, -3, 7, 4)
      val a = Matrix(4, initialValues)

      And("B ← inverse(A)")
      val b = inverse(a)

      Then("determinant(A) = 532")
      assert(determinant(a) == 532)

      And("cofactor(A, 2, 3) = -160")
      assert(cofactor(a, 2, 3) == -160)

      And("B[3,2] = -160/532")
      val n: Double = -160
      val d: Double = 532
      assert(b.matrix(3)(2) == n / d)

      And("cofactor(A, 3, 2) = 105")
      assert(cofactor(a, 3, 2) == 105)

      And("B[2,3] = 105/532")
      val nn: Double = 105
      val dd: Double = 532
      assert(b.matrix(2)(3) == nn / dd)

      And(
        "B is the following 4x4 matrix:\n" +
          "|  0.21805 |  0.45113 |  0.24060 | -0.04511 |\n" +
          "| -0.80827 | -1.45677 | -0.44361 |  0.52068 |\n" +
          "| -0.07895 | -0.22368 | -0.05263 |  0.19737 |\n" +
          "| -0.52256 | -0.81391 | -0.30075 |  0.30639 |"
      )
      val expectedInitialValues =
        Vector[Double](0.21805, 0.45113, 0.24060, -0.04511, -0.80827, -1.45677, -0.44361, 0.52068, -0.07895, -0.22368,
          -0.05263, 0.19737, -0.52256, -0.81391, -0.30075, 0.30639)
      val expected = Matrix(4, expectedInitialValues)

      assert(b == expected)
    }

    Scenario("Calculating the inverse of another matrix") {
      Given(
        "the following 4x4 matrix A:\n" +
          "|  8 | -5 |  9 |  2 |\n" +
          "|  7 |  5 |  6 |  1 |\n" +
          "| -6 |  0 |  9 |  6 |\n" +
          "| -3 |  0 | -9 | -4 |"
      )
      val initialValues =
        Vector[Double](8, -5, 9, 2, 7, 5, 6, 1, -6, 0, 9, 6, -3, 0, -9, -4)
      val a = Matrix(4, initialValues)

      And("B ← inverse(A)")
      val b = inverse(a)

      Then(
        "inverse(A) is the following 4x4 matrix\n" +
          "| -0.15385 | -0.15385 | -0.28205 | -0.53846 |\n" +
          "| -0.07692 |  0.12308 |  0.02564 |  0.03077 |\n" +
          "|  0.35897 |  0.35897 |  0.43590 |  0.92308 |\n" +
          "| -0.69231 | -0.69231 | -0.76923 | -1.92308 |"
      )
      val expectedInitialValues =
        Vector[Double](-0.15385, -0.15385, -0.28205, -0.53846, -0.07692, 0.12308, 0.02564, 0.03077, 0.35897, 0.35897,
          0.43590, 0.92308, -0.69231, -0.69231, -0.76923, -1.92308)
      val expected = Matrix(4, expectedInitialValues)

      assert(b == expected)
    }

    Scenario("Calculating the inverse of a third matrix") {
      Given(
        "the following 4x4 matrix A:\n" +
          "|  9 |  3 |  0 |  9 |\n" +
          "| -5 | -2 | -6 | -3 |\n" +
          "| -4 |  9 |  6 |  4 |\n" +
          "| -7 |  6 |  6 |  2 |"
      )
      val initialValues =
        Vector[Double](9, 3, 0, 9, -5, -2, -6, -3, -4, 9, 6, 4, -7, 6, 6, 2)
      val a = Matrix(4, initialValues)

      And("B ← inverse(A)")
      val b = inverse(a)

      Then(
        "inverse(A) is the following 4x4 matrix\n" +
          "| -0.04074 | -0.07778 |  0.14444 | -0.22222 |\n" +
          "| -0.07778 |  0.03333 |  0.36667 | -0.33333 |\n" +
          "| -0.02901 | -0.14630 | -0.10926 |  0.12963 |\n" +
          "|  0.17778 |  0.06667 | -0.26667 |  0.33333 |"
      )
      val expectedInitialValues =
        Vector[Double](-0.04074, -0.07778, 0.14444, -0.22222, -0.07778, 0.03333, 0.36667, -0.33333, -0.02901, -0.14630,
          -0.10926, 0.12963, 0.17778, 0.06667, -0.26667, 0.33333)
      val expected = Matrix(4, expectedInitialValues)

      assert(b == expected)
    }

    Scenario("Multiplying a product by its inverse") {
      Given(
        "the following 4x4 matrix A:\n" +
          "| -3 | -9 |  7 |  3 |\n" +
          "|  3 | -8 |  2 | -9 |\n" +
          "| -4 |  4 |  4 |  1 |\n" +
          "| -6 |  5 | -1 |  1 |"
      )
      val initialValuesA =
        Vector[Double](-3, -9, 7, 3, 3, -8, 2, -9, -4, 4, 4, 1, -6, 5, -1, 1)
      val a = Matrix(4, initialValuesA)

      And(
        "the following 4x4 matrix B:\n" +
          "| 8 |  2 | 2 | 2 |\n" +
          "| 3 | -1 | 7 | 0 |\n" +
          "| 7 |  0 | 5 | 4 |\n" +
          "| 6 | -2 | 0 | 5 |"
      )
      val initialValuesB =
        Vector[Double](8, 2, 2, 2, 3, -1, 7, 0, 7, 0, 5, 4, 6, -2, 0, 5)
      val b = Matrix(4, initialValuesB)

      And("C ← A * B")
      val c = a * b

      Then("C * inverse(B) = A")
      assert(c * inverse(b) == a)
    }
  }
}
