package com.craigtreptow.scrayz

import Builders.{point, vector}
import RayUtils.{reflect}

class ColorsFeature extends FeatureSpec {
  Feature("Colors") {
    Scenario("Colors are (red, green, blue) tuples") {
      Given("c ← color(-0.5, 0.4, 1.7)")
      val c = Color(red = -0.5, green = 0.4, blue = 1.7)

      Then("c.red = -0.5")
      assert(c.red == -0.5)

      Then("c.green = 0.4")
      assert(c.green == 0.4)

      Then("c.blue = 1.7")
      assert(c.blue == 1.7)
    }

    Scenario("Adding colors") {
      Given("c1 ← color(0.9, 0.6, 0.75)")
      val c1 = Color(red = 0.9, green = 0.6, blue = 0.75)

      Given("c2 ← color(0.7, 0.1, 0.25)")
      val c2 = Color(red = 0.7, green = 0.1, blue = 0.25)

      Then("c1 + c2 = color(1.6, 0.7, 1.0)")
      val computed = c1 + c2
      val expected = Color(red = 1.6, green = 0.7, blue = 1.0)
      assert(computed == expected)
    }

    Scenario("Subtracting colors") {
      Given("c1 ← color(0.9, 0.6, 0.75)")
      val c1 = Color(red = 0.9, green = 0.6, blue = 0.75)

      Given("c2 ← color(0.7, 0.1, 0.25)")
      val c2 = Color(red = 0.7, green = 0.1, blue = 0.25)

      Then("c1 - c2 = color(0.2, 0.5, 0.5)")
      val computed = c1 - c2
      val expected = Color(red = 0.2, green = 0.5, blue = 0.5)
      assert(computed == expected)
    }

    Scenario("Multiplying a color by a scalar") {
      Given("c ← color(0.2, 0.3, 0.4)")
      val c = Color(red = 0.2, green = 0.3, blue = 0.4)

      Then("c * 2 = color(0.4, 0.6 0.8)")
      val computed = c * 2
      val expected = Color(red = 0.4, green = 0.6, blue = 0.8)
      assert(computed == expected)
    }

    Scenario("Multiplying colors") {
      Given("c1 ← color(1, 0.2, 0.4)")
      val c1 = Color(red = 1, green = 0.2, blue = 0.4)

      Given("c2 ← color(0.9, 1, 0.1)")
      val c2 = Color(red = 0.9, green = 1, blue = 0.1)

      Then("c1 * c2 = color(0.9, 0.2, 0.04)")
      val computed = c1 * c2
      val expected = Color(red = 0.9, green = 0.2, blue = 0.04)
      assert(computed == expected)
    }

    Scenario("Reflecting a vector approaching at 45°") {
      Given("v ← vector(1, -1, 0)")
      val v = vector(1, -1, 0)

      And("n ← vector(0, 1, 0)")
      val n = vector(0, 1, 0)

      When("r ← reflect(v, n)")
      val reflected = reflect(in = v, normal = n)

      Then("r = vector(1, 1, 0)")
      val expected = vector(1, 1, 0)
      assert(reflected == expected)
    }

    Scenario("Reflecting a vector off a slanted surface") {
      Given("v ← vector(0, -1, 0)")
      val v = vector(0, -1, 0)

      And("n ← vector(√2/2, √2/2, 0)")
      val sqrt = Math.sqrt(2)
      val sqrtOverTwo = sqrt / 2
      val n = vector(sqrtOverTwo, sqrtOverTwo, 0)

      When("r ← reflect(v, n)")
      val reflected = reflect(in = v, normal = n)

      Then("r = vector(1, 0, 0)")
      val expected = vector(1, 0, 0)
      assert(reflected == expected)
    }
  }
}
