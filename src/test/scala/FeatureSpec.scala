package com.craigtreptow.scrayz

import org.scalatest._
import org.scalatest.featurespec._

abstract class FeatureSpec extends AnyFeatureSpec with GivenWhenThen
