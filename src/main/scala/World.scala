package com.craigtreptow.scrayz

case class World(val objects: List[Sphere], val light: PointLight)
