package com.craigtreptow.scrayz

import Builders.{point, vector}
import com.craigtreptow.scrayz.{RayUtils => RU}
import MatrixUtils._
import MaterialUtils._
import CanvasUtils.{canvasToPPM, writePixelAt, canvasToLines, ppmBody}

object Chapter6:
  def castSphere(fileName: String) =
    val canvasPixels = 500
    var canvas = Canvas(width = canvasPixels, height = canvasPixels)

    val pinkish = Color(red = 1, green = 0.2, blue = 1)
    val material = Material(color = pinkish)
    val shape = Sphere(material = material)

    val lightPosition = point(-10, 10, -10)
    val white = Color(red = 1, green = 1, blue = 1)
    val lightColor = white
    val light = PointLight(position = lightPosition, intensity = lightColor)

    // start the ray at z = -5
    val rayOrigin = point(0, 0, -5)
    // put the wall at z = 10
    val wallZ = 10
    // following the ray to the wall gives +/- 3, so use 7 for a margin
    val wallSize = 7.0
    // divide wall size by number of pixels to get size of single pixel (in world space units)
    val pixelSize = wallSize / canvasPixels
    // assume you're looking directly at the center of the sphere
    // half of the wall is to the left and half is to the right
    // since the wall is centered around the origin, so
    // 'half' describes the min and max x and y coordinates of the wall
    val half = wallSize / 2

    // # for each row of pixels in the canvas​
    for y <- 0 to canvasPixels - 1 do
      // ​compute the world y coordinate (top = +half, bottom = -half)​
      val worldY = half - pixelSize * y

      // for each pixel in the row
      for x <- 0 to canvasPixels - 1 do
        // compute the world x coordinate (left = -half, right = half)​
        val worldX = -half + pixelSize * x

        // describe the point on the wall that the ray will target​
        val position = point(worldX, worldY, wallZ)

        val r = Ray(rayOrigin, (position - rayOrigin).normalize)
        val xs = RU.intersect(shape, r)
        if xs.nonEmpty then
          val (intersectedObject, tValue) = RU.hit(xs) match
            case Some(h) => (h.intersectedObject, h.tValue)
            case (None)  => (shape, 0.0)

          val p = RU.position(r, tValue)
          val normal = RU.normalAt(sphere = intersectedObject, p)
          val eye = r.direction.negate

          val newColor = lighting(intersectedObject.material, light, p, eye, normal)

          canvas = writePixelAt(newColor, x, y, canvas)

    canvasToPPM(canvas, fileName)
