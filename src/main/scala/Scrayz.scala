package com.craigtreptow.scrayz

import Chapter1.{simulateProjectile}
import Chapter2.{plotSimulatedProjectile}
import Chapter4.{plotClockFace}
import Chapter5.{castCircle}
import Chapter6.{castSphere}
import Chapter7.{renderScene}

@main def scrayz() =
  val chapters = List(
    (
      simulateProjectile,
      "Chapter1: Calculate and show locations of projectile that falls until it hits the ground",
      ""
    ),
    (
      plotSimulatedProjectile,
      "Chapter2: Simulate shooting a projectile that falls until it hits the ground",
      "ProjectilePlot.ppm"
    ),
    (
      plotClockFace,
      "Chapter4: Creates points and rotates them to plot a clock face",
      "ClockFace.ppm"
    ),
    (castCircle, "Chapter5: Casting a ray onto a red circle. Rays that hit the sphere are red, those that miss are black", "Circle.ppm"),
    (castSphere, "Chapter6: Casting a ray onto a pinkish sphere", "Sphere.ppm"),
    (
      renderScene,
      "Chapter7: Rendering a scene with a floor, two walls, and three sphere",
      "ThreeSpheresScene.ppm"
    )
  )

  // clear the terminal
  // Unicode ESC (001b)
  // ANSI escape code ([2J) J clears the entire screen, 2 does it from top to bottom
  print("\u001b[2J")
  println("Running end of chapter programs\n")
  chapters.map(c => runChapter(func = c._1, desc = c._2, fileName = c._3))
  println("All chapters finished\n")

  def runChapter(func: String => Unit, desc: String, fileName: String) =
    println(s"$desc")
    val s = System.currentTimeMillis
    func(fileName)
    println(s"Duration: ${millisToSeconds(System.currentTimeMillis - s)} seconds")
    println(s"${finishedMessage(fileName)}")

  def millisToSeconds(milliseconds: Long): Double = milliseconds.toDouble / 1000.0

  def finishedMessage(fileName: String): String = if fileName == "" then "Finished\n" else s"Finished, wrote $fileName\n"
