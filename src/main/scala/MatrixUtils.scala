package com.craigtreptow.scrayz

import scala.math.{sin, cos}

object MatrixUtils:
  def viewTransform(from: Tuple, to: Tuple, up: Tuple): Matrix =
    val forward = (to - from).normalize
    val left = forward cross (up.normalize)
    val trueUp = left cross forward
    val initialValues = Vector[Double](
      left.x,
      left.y,
      left.z,
      0,
      trueUp.x,
      trueUp.y,
      trueUp.z,
      0,
      -forward.x,
      -forward.y,
      -forward.z,
      0,
      0,
      0,
      0,
      1
    )
    val orientation = Matrix(4, initialValues)
    val t = translation(-from.x, -from.y, -from.z)
    orientation * t

  def shearing(
      x_proportion_y: Double,
      x_proportion_z: Double,
      y_proportion_x: Double,
      y_proportion_z: Double,
      z_proportion_x: Double,
      z_proportion_y: Double
  ): Matrix =
    val i = identityMatrix()
    val vals =
      i.orderedValues
        .updated(1, x_proportion_y)
        .updated(2, x_proportion_z)
        .updated(4, y_proportion_x)
        .updated(6, y_proportion_z)
        .updated(8, z_proportion_x)
        .updated(9, z_proportion_y)
    Matrix(4, vals)

  def rotationZ(radians: Double): Matrix =
    val i = identityMatrix()
    val vals =
      i.orderedValues
        .updated(0, cos(radians))
        .updated(1, -(sin(radians)))
        .updated(4, sin(radians))
        .updated(5, cos(radians))
    Matrix(4, vals)

  def rotationY(radians: Double): Matrix =
    val i = identityMatrix()
    val vals =
      i.orderedValues
        .updated(0, cos(radians))
        .updated(2, sin(radians))
        .updated(8, -(sin(radians)))
        .updated(10, cos(radians))
    Matrix(4, vals)

  def rotationX(radians: Double): Matrix =
    val i = identityMatrix()
    val vals =
      i.orderedValues
        .updated(5, cos(radians))
        .updated(6, -(sin(radians)))
        .updated(9, sin(radians))
        .updated(10, cos(radians))
    Matrix(4, vals)

  def scaling(x: Double, y: Double, z: Double): Matrix =
    val i = identityMatrix()
    val vals = i.orderedValues.updated(0, x).updated(5, y).updated(10, z)
    Matrix(4, vals)

  def translation(x: Double, y: Double, z: Double): Matrix =
    val i = identityMatrix()
    val vals =
      i.orderedValues.updated(3, x).updated(7, y).updated(11, z)
    Matrix(4, vals)

  def inverse(m: Matrix): Matrix =
    if !invertible(m) then m
    else
      val d = determinant(m)
      val values = for
        i <- 0 until m.size
        j <- 0 until m.size
      yield cofactor(m, j, i) / d

      Matrix(4, values.toVector)

  def invertible(m: Matrix): Boolean =
    determinant(m) != 0

  def cofactor(m: Matrix, row: Int, column: Int): Double =
    val mnr = minor(m, row, column)
    if isOdd(row + column) then -mnr else mnr

  // Rules for negating
  // + - +
  // - + -
  // + - +
  // Easier: negate if row + column is odd

  def isOdd(i: Int): Boolean = i % 2 != 0

  def minor(m: Matrix, row: Int, column: Int): Double = determinant(
    submatrix(m, row, column)
  )

  def submatrix(m: Matrix, row: Int, column: Int): Matrix =
    val is = List.range(0, m.size).filter(_ != row)
    val js = List.range(0, m.size).filter(_ != column)
    val values = for
      i <- is
      j <- js
    yield m.matrix(i)(j)

    Matrix(m.size - 1, values.toVector)

  def determinant(m: Matrix): Double =
    //00 01 --> a b
    //10 11 --> c d
    if m.size == 2 then m.matrix(0)(0) * m.matrix(1)(1) - m.matrix(0)(1) * m.matrix(1)(0)
    else
      val xs =
        for col <- 0 until m.size
        yield m.matrix(0)(col) * cofactor(m, 0, col)

      xs.sum

  def transpose(m: Matrix): Matrix =
    val values = for
      i <- 0 until m.size
      j <- 0 until m.size
    yield
    // note the j & i swap to process column by column
    m.matrix(j)(i)

    Matrix(m.size, values.toVector)

  def identityMatrix(): Matrix =
    val initialValues =
      Vector[Double](1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)

    Matrix(4, initialValues)
