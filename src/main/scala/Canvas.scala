package com.craigtreptow.scrayz

case class Canvas(val width: Int, val height: Int, val grid: CanvasGridType) {}

object Canvas:
  def apply(width: Int, height: Int, color: Color = DefaultColor): Canvas =
    val grid = Vector.fill(width * height)(color)
    val c = new Canvas(width, height, grid)
    c
