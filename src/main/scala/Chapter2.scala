package com.craigtreptow.scrayz

import Builders.{point, vector}
import CanvasUtils.{canvasToPPM, writePixelAt, canvasToLines, ppmBody}

object Chapter2:
  def plotSimulatedProjectile(fileName: String) =
    val startingPoint = point(0, 1, 0)
    val velocity = vector(1, 1.8, 0).normalize * 11.25
    val projectile = (startingPoint, velocity)

    val canvas = Canvas(width = 900, height = 550)
    val red = Color(red = 1, blue = 0, green = 0)
    val gravity = vector(0, -0.1, 0)
    val wind = vector(-0.01, 0, 0)
    val environment = (gravity, wind)

    val newCanvas = tick(environment, projectile, canvas, red)

    canvasToPPM(newCanvas, fileName)

  private def tick(
      environment: Tuple2[Tuple, Tuple],
      projectile: Tuple2[Tuple, Tuple],
      canvas: Canvas,
      color: Color
  ): Canvas =
    val (gravity, wind) = environment
    val (position, velocity) = projectile

    val newPosition: Tuple = position + velocity
    val newVelocity: Tuple = velocity + gravity + wind
    val newProjectile = (newPosition, newVelocity)

    if newPosition.y <= 0 then canvas
    else
      val newCanvas = writePixelAt(color, projectile._1.x.round.toInt, canvas.height - projectile._1.y.round.toInt, canvas)
      tick(environment, newProjectile, newCanvas, color)
