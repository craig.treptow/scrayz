package com.craigtreptow.scrayz

import java.util.UUID.randomUUID
import MatrixUtils._

sealed trait Shape
case class Sphere(
                   val id: String = randomUUID().toString,
                   val transform: Matrix = identityMatrix(),
                   val material: Material = Material()
) extends Shape
    derives CanEqual:

  // this only works here and not as an extension method
  def ==(that: Sphere): Boolean =
    val transformResult = this.transform == that.transform
    val materialResult = this.material == that.material
    val finalResult = transformResult && materialResult
    finalResult

  def setTransform(transform: Matrix): Sphere =
    this.copy(transform = transform)