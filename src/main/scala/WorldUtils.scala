package com.craigtreptow.scrayz

import scala.math.{sin, cos}

import RayUtils.{intersect, position, normalAt, hit, intersections}
import MaterialUtils.{lighting}

object WorldUtils:
  def isShadowed(world: World, point: Tuple): Boolean =
    val v = world.light.position - point
    val distance = v.magnitude
    val direction = v.normalize
    val r = Ray(origin = point, direction = direction)
    val intersections = intersectWorld(world = world, ray = r)
    println(s"intersections: $intersections")
    val h = hit(intersections.toVector)
    println(s"            h: $h")

    h match
      case Some(h) => if h.tValue < distance then true else false
      case None    => false

  def colorAt(world: World, ray: Ray): Color =
    val black = Color(0, 0, 0)
    val intersections = intersectWorld(world, ray)
    val i = hit(intersections.toVector)
    i match
      case None => black
      case Some(result) =>
        val cs = prepareComputations(intersection = result, ray = ray)
        shadeHit(w = world, comps = cs)

  def intersectWorld(world: World, ray: Ray): List[Intersection] =
    val objs = world.objects
    val intersections = for (obj <- objs) yield intersect(sphere = obj, ray = ray)
    intersections.flatten.sortBy(_.tValue)

  def shadeHit(w: World, comps: Computations): Color =

    val shadowedPoint = isShadowed(world = w, point = comps.overPoint)

    lighting(
      material = comps.calculatedObject.material,
      light = w.light,
      point = comps.overPoint,
      eyeV = comps.calculatedEyeVector,
      normalV = comps.calculatedNormalVector,
      shadowed = shadowedPoint
    )

  def prepareComputations(intersection: Intersection, ray: Ray): Computations =
    val tValue = intersection.tValue
    val obj = intersection.intersectedObject
    val pos = position(ray, tValue)
    val eyeV = ray.direction.negate
    var normalV = normalAt(obj, pos)
    val isInside = calcInside(normalV, eyeV)
    if isInside then normalV = normalV.negate
    val overPnt = pos + normalV * EPSILON

    Computations(
      calculatedtValue = tValue,
      calculatedObject = obj,
      calculatedPoint = pos,
      calculatedEyeVector = eyeV,
      calculatedNormalVector = normalV,
      inside = isInside,
      overPoint = overPnt
    )

private def calcInside(nv: Tuple, ev: Tuple): Boolean =
  val d = nv dot ev
  if d < 0 then true else false
