package com.craigtreptow.scrayz

import com.craigtreptow.scrayz.{Utils => U}
import Builders.{vector}

case class Tuple(x: Double, y: Double, z: Double, w: Double):
  // this only works here and not as an extension method
  def ==(that: Tuple): Boolean =
    val xResult = U.equals(this.x, that.x)
    val yResult = U.equals(this.y, that.y)
    val zResult = U.equals(this.z, that.z)
    val finalResult = xResult && yResult && zResult
    finalResult

  def +(that: Tuple): Tuple =
    this.copy(x = this.x + that.x, y = this.y + that.y, z = this.z + that.z, w = this.w + that.w)

  def -(that: Tuple): Tuple =
    this.copy(x = this.x - that.x, y = this.y - that.y, z = this.z - that.z, w = this.w - that.w)

  def *(d: Double): Tuple = this.copy(x = this.x * d, y = this.y * d, z = this.z * d, w = this.w * d)

  def /(that: Double): Tuple = this.copy(x = this.x / that, y = this.y / that, z = this.z / that, w = this.w / that)

  // TODO: using -: produces '=' expected, but identifier found
  def negate: Tuple = this.copy(x = -this.x, y = -this.y, z = -this.z, w = -this.w)

  def isPoint: Boolean = this.w == 1.0

  def isVector: Boolean = this.w == 0.0

  def magnitude: Double = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w)

  def normalize: Tuple =
    val m = this.magnitude
    vector(x = this.x / m, y = this.y / m, z = this.z / m)

  def dot(that: Tuple): Double = this.x * that.x + this.y * that.y + this.z * that.z + this.w * that.w

  def cross(that: Tuple): Tuple =
    vector(
      x = this.y * that.z - this.z * that.y,
      y = this.z * that.x - this.x * that.z,
      z = this.x * that.y - this.y * that.x
    )
