package com.craigtreptow.scrayz

import com.craigtreptow.scrayz.CanvasUtils._
import WorldUtils.{colorAt}
import RayUtils.{rayForPixel}

object Utils:
  def render(camera: Camera, world: World): Canvas =
    val fileName = "Circle.ppm"
    var canvas = Canvas(width = camera.hSize.toInt, height = camera.vSize.toInt)

    for y <- 0 to camera.vSize.toInt - 1 do
      for x <- 0 to camera.hSize.toInt - 1 do
        val ray = rayForPixel(camera = camera, px = x, py = y)
        val color = colorAt(world = world, ray = ray)
        canvas = writePixelAt(color, x, y, canvas)

    canvas

  def equals(a: Double, b: Double): Boolean =
    // so far, I haven't needed to actually use this.  The comparison
    // below is working fine.  Plus, this would be slower, since
    // BigDecimal converts to a String and converts back, I believe.
    //
    // val roundedA = BigDecimal(a).setScale(5, BigDecimal.RoundingMode.HALF_UP)
    // val roundedB = BigDecimal(b).setScale(5, BigDecimal.RoundingMode.HALF_UP)
    // val result = roundedA == roundedB
    // result

    val ab = (a - b).abs
    if ((a - b).abs < EPSILON) true else false

  type canvasRow = Array[Color]
  type canvasMatrix = Array[canvasRow]

  def printCanvas(c: Canvas) =
    println(s"Width: ${c.width} Height: ${c.height}")
    for {
      y <- 0 until c.height
      x <- 0 until c.width
    } {
      if (x == c.width - 1)
        println(s"($x, $y) -> ${colorValues(pixelAt(x, y, c))}")
      else print(s"($x, $y) -> ${colorValues(pixelAt(x, y, c))} ")
    }

  def printCanvasHelper(
      x: Int,
      y: Int,
      maxW: Int,
      maxH: Int,
      c: canvasMatrix
  ): Unit =
    if x > maxW - 2 then
      // ultimate color in row
      println(s"($x, $y) -> ${c(x)(y)}")
      if y < maxH - 1 then printCanvasHelper(x = 0, y = y + 1, maxW = maxW, maxH = maxH, c)
    else
      // 1 - penultimate color of row
      print(s"($x, $y) -> ${c(x)(y)} ")
      printCanvasHelper(x = x + 1, y = y, maxW = maxW, maxH = maxH, c)
