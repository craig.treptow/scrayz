package com.craigtreptow.scrayz

case class Intersection(val tValue: Double, val intersectedObject: Sphere)
