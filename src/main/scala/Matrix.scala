package com.craigtreptow.scrayz

import com.craigtreptow.scrayz.{Utils => U}

case class Matrix(val size: Int, val orderedValues: Vector[Double], val matrix: MatrixType):
  //    0   1   2   3
  // 0 a_a a_b a_c a_d
  // 1 b_a b_b b_c b_d
  // 2 c_a c_b c_c c_d
  // 3 d_a d_b d_c d_d

  def ==(that: Matrix): Boolean =
    if (this.size != that.size) return false

    val results = for
      i <- 0 until this.size
      j <- 0 until this.size
    yield U.equals(this.matrix(i)(j), that.matrix(i)(j))

    !results.contains(false)

  def *(that: Matrix): Matrix =
    //00 01 02 03
    //10 11 12 13
    //20 21 22 23
    //30 31 32 33

    var m = Array.ofDim[Double](size, size)
    val orderedValues = for
      i <- 0 until this.size
      j <- 0 until this.size
    yield this.matrix(i)(0) * that.matrix(0)(j) +
      this.matrix(i)(1) * that.matrix(1)(j) +
      this.matrix(i)(2) * that.matrix(2)(j) +
      this.matrix(i)(3) * that.matrix(3)(j)

    Matrix(this.size, orderedValues.toVector)

  def *(that: Tuple): Tuple =
    //00 01 02 03
    //10 11 12 13
    //20 21 22 23
    //30 31 32 33

    var m = Array.ofDim[Double](size, size)
    val orderedValues = for
      i <- 0 until this.size
      j <- 0 until this.size
    yield this.matrix(i)(0) * that.x +
      this.matrix(i)(1) * that.y +
      this.matrix(i)(2) * that.z +
      this.matrix(i)(3) * that.w

    val tX = (this.matrix(0)(0) * that.x) + (this.matrix(0)(1) * that.y) + (this
      .matrix(0)(2) * that.z) + (this.matrix(0)(3) * that.w)
    val tY = (this.matrix(1)(0) * that.x) + (this.matrix(1)(1) * that.y) + (this
      .matrix(1)(2) * that.z) + (this.matrix(1)(3) * that.w)
    val tZ = (this.matrix(2)(0) * that.x) + (this.matrix(2)(1) * that.y) + (this
      .matrix(2)(2) * that.z) + (this.matrix(2)(3) * that.w)
    val tW = (this.matrix(3)(0) * that.x) + (this.matrix(3)(1) * that.y) + (this
      .matrix(3)(2) * that.z) + (this.matrix(3)(3) * that.w)

    Tuple(x = tX, y = tY, z = tZ, w = tW)

object Matrix:
  def apply(size: Int, orderedValues: Vector[Double]): Matrix =
    var m = Array.ofDim[Double](size, size)
    for
      i <- 0 until size
      j <- 0 until size
    do m(i)(j) = orderedValues(j + (i * size))

    val matrix = new Matrix(size, orderedValues, m)
    matrix
