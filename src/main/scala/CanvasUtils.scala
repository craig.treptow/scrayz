package com.craigtreptow.scrayz

import java.io._

object CanvasUtils:
  type linesType = Vector[String]

  def pixelAt(x: Int, y: Int, canvas: Canvas): Color =
    val grid = canvas.grid
    val index = x + (y * canvas.width)
    grid(index)

  def writePixelAt(newColor: Color, x: Int, y: Int, canvas: Canvas): Canvas =
    val index = x + (y * canvas.width)
    val updatedGrid = canvas.grid.updated(index, newColor)
    canvas.copy(grid = updatedGrid)

  def canvasToPPM(canvas: Canvas, fileName: String): Unit =
    writeFile(lines = canvasToLines(canvas), fileName)

  def canvasToLines(canvas: Canvas): linesType =
    val ppmVersion = "P3"
    val imageDimensions = s"${canvas.width} ${canvas.height}"
    val maxColorValue = "255"
    val ppm = Vector(
      ppmVersion,
      "\n",
      imageDimensions,
      "\n",
      maxColorValue,
      "\n"
    ) ++ ppmBody(canvas) :+ "\n"
    ppm

  def ppmBody(canvas: Canvas): Vector[String] =
    val answer = for
      y <- 0 until canvas.height
      x <- 0 until canvas.width
    yield s"${colorValues(pixelAt(x, y, canvas))} "
    answer
      .grouped(canvas.width)
      .toVector
      .map(_.mkString)

  def writeFile(lines: linesType, fileName: String) =
    val file = new File(fileName)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(lines.mkString)
    bw.close()

  def colorValues(color: Color) =
    s"${clamp(color.red)} ${clamp(color.green)} ${clamp(color.blue)}"

  def clamp(colorValue: Double): Int =
    val min = 0
    val max = 255
    val adjustedValue = Math.round((colorValue * (max + 0.5))).toInt

    adjustedValue match
      case x if x < min => min
      case x if x > max => max
      case x            => x
