package com.craigtreptow.scrayz

import Builders.{point, vector}

object Chapter1:
  def simulateProjectile(fileName: String) =
    // gravity -0.1 unit/tick, and wind is -0.01 unit/tick
    val gravity = vector(0, -0.1, 0)
    val wind = vector(-0.01, 0, 0)
    val environment = (gravity, wind)

    // projectile starts one unit above the origin
    // velocity is normalized to 1 unit/tick
    val position = point(0, 1, 0)
    val velocity = vector(1, 1, 0).normalize
    val projectile = (position, velocity)

    val initialProjectiles = List[Tuple2[Tuple, Tuple]]()
    val newProjectiles = tick(environment, projectile, initialProjectiles)

    for (projectile <- newProjectiles) println(projectile._1.y)
    val numberOfTicks = newProjectiles.length
    println(s"\nIt took $numberOfTicks ticks to hit the ground")

  private def tick(
      environment: Tuple2[Tuple, Tuple],
      projectile: Tuple2[Tuple, Tuple],
      projectiles: List[Tuple2[Tuple, Tuple]]
  ): List[Tuple2[Tuple, Tuple]] =
    val (gravity, wind) = environment
    val (position, velocity) = projectile

    val newPosition: Tuple = position + velocity
    val newVelocity: Tuple = velocity + gravity + wind
    val newProjectile = (newPosition, newVelocity)

    if newPosition.y <= 0 then projectiles.reverse
    else
      val newProjectiles = newProjectile :: projectiles
      tick(environment, newProjectile, newProjectiles)
