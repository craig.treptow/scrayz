package com.craigtreptow.scrayz

import MatrixUtils.{identityMatrix}

case class Camera(val hSize: Double, val vSize: Double, val fieldOfView: Double, transform: Matrix = identityMatrix()):
  def pixelSize: Double = (halfWidth * 2) / hSize

  def halfWidth: Double = if aspect >= 1 then halfView else halfView * aspect

  def halfHeight: Double = if aspect >= 1 then halfView / aspect else halfView

  private def halfView: Double = Math.tan(this.fieldOfView / 2)

  private def aspect: Double = this.hSize / this.vSize
