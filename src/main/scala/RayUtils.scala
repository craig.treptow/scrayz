package com.craigtreptow.scrayz

import Builders.{point, vector}
import MatrixUtils._

object RayUtils:
  def rayForPixel(camera: Camera, px: Double, py: Double): Ray =
    // the offset from the edge of the canvas to the pixel's center
    val xOffset = (px + 0.5) * camera.pixelSize
    val yOffset = (py + 0.5) * camera.pixelSize

    // the untransformed coordinates of the pixel in world space
    // the camera looks toward -z, so +x is to the *left*
    val worldX = camera.halfWidth - xOffset
    val worldY = camera.halfHeight - yOffset

    // using the camera matrix, transform the canvas point and the origin,
    // and then compute the ray's direction vector
    // remember that the canvas is at z = -1
    val pixel = inverse(camera.transform) * point(worldX, worldY, -1)
    val origin = inverse(camera.transform) * point(0, 0, 0)
    val direction = (pixel - origin).normalize

    Ray(origin = origin, direction = direction)

  def position(ray: Ray, t: Double): Tuple = ray.origin + (ray.direction * t)

  def intersect(sphere: Sphere, ray: Ray): Vector[Intersection] =
    // every sphere's center is at the world origin, point(0,0.0)
    // all spheres have radii of 1
    val transformedRay = transform(ray, inverse(sphere.transform))
    val sphereToRay = transformedRay.origin - point(0, 0, 0)
    val a = transformedRay.direction dot transformedRay.direction
    val b = 2 * (transformedRay.direction dot sphereToRay)
    val c = (sphereToRay dot sphereToRay) - 1
    val discriminant = (b * b) - 4 * a * c

    // Vector, so it is indexed and immutable
    if discriminant < 0 then Vector()
    else
      val t1 = (-b - Math.sqrt(discriminant)) / (2 * a)
      val t2 = (-b + Math.sqrt(discriminant)) / (2 * a)
      Vector(Intersection(t1, sphere), Intersection(t2, sphere))

  def intersections(l: List[Intersection]): Vector[Intersection] =
    l.sortBy(_.tValue).toVector

  def transform(ray: Ray, matrix: Matrix): Ray =
    val p = matrix * ray.origin
    val v = matrix * ray.direction
    Ray(origin = p, direction = v)

  def normalAt(sphere: Sphere, worldPoint: Tuple): Tuple =
    val invTransform = inverse(sphere.transform)
    val objectPoint = invTransform * worldPoint
    val origin = point(0, 0, 0)
    val objectNormal = objectPoint - origin
    val worldNormal = transpose(invTransform) * objectNormal
    // hack mentioned in the book
    // we *should* be finding submatrix(transform, 3, 3) first
    // and multiplying by the inverse and transpose of that
    val wn = worldNormal.copy(w = 0)
    val wnn = wn.normalize
    wnn

  def reflect(in: Tuple, normal: Tuple): Tuple =
    in - normal * 2 * (in dot normal)

  def hit(intersections: Vector[Intersection]): Option[Intersection] =
    if intersections.isEmpty then None
    else
      intersections.isEmpty match
        case true  => None
        case false => findHit(intersections)

  private def findHit(intersections: Vector[Intersection]): Option[Intersection] =
    val ints = intersections.filter(_.tValue >= 0).sortBy(_.tValue)

    if ints.isEmpty then None
    else Some(ints.head)
